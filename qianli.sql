-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 29 août 2019 à 09:22
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `qianli`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `table`, `name`) VALUES
(1, 'cat_stencils', 'Stencils'),
(2, 'cat_removers', 'Removers'),
(3, 'cat_screwdrivers', 'Screwdrivers'),
(4, 'cat_datacopy', 'Data and ID Copying tools'),
(5, 'cat_welding', 'Welding supplies'),
(6, 'cat_pryers', 'Prying tools');

-- --------------------------------------------------------

--
-- Structure de la table `cat_datacopy`
--

DROP TABLE IF EXISTS `cat_datacopy`;
CREATE TABLE IF NOT EXISTS `cat_datacopy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_datacopy`
--

INSERT INTO `cat_datacopy` (`id`, `product_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `cat_pryers`
--

DROP TABLE IF EXISTS `cat_pryers`;
CREATE TABLE IF NOT EXISTS `cat_pryers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_pryers`
--

INSERT INTO `cat_pryers` (`id`, `product_id`) VALUES
(1, 2),
(2, 10),
(3, 24);

-- --------------------------------------------------------

--
-- Structure de la table `cat_removers`
--

DROP TABLE IF EXISTS `cat_removers`;
CREATE TABLE IF NOT EXISTS `cat_removers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_removers`
--

INSERT INTO `cat_removers` (`id`, `product_id`) VALUES
(1, 14),
(4, 26),
(3, 25),
(5, 27);

-- --------------------------------------------------------

--
-- Structure de la table `cat_screwdrivers`
--

DROP TABLE IF EXISTS `cat_screwdrivers`;
CREATE TABLE IF NOT EXISTS `cat_screwdrivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_screwdrivers`
--

INSERT INTO `cat_screwdrivers` (`id`, `product_id`) VALUES
(3, 8);

-- --------------------------------------------------------

--
-- Structure de la table `cat_stencils`
--

DROP TABLE IF EXISTS `cat_stencils`;
CREATE TABLE IF NOT EXISTS `cat_stencils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_stencils`
--

INSERT INTO `cat_stencils` (`id`, `product_id`) VALUES
(1, 6),
(2, 7),
(3, 11),
(5, 17),
(6, 19),
(8, 28),
(9, 29),
(10, 30),
(11, 31),
(12, 32),
(13, 33),
(14, 34),
(15, 35),
(16, 36),
(17, 37),
(18, 41),
(19, 42),
(20, 43),
(21, 47),
(22, 40),
(23, 44),
(24, 45),
(25, 46),
(26, 48),
(27, 38),
(28, 39);

-- --------------------------------------------------------

--
-- Structure de la table `cat_welding`
--

DROP TABLE IF EXISTS `cat_welding`;
CREATE TABLE IF NOT EXISTS `cat_welding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_welding`
--

INSERT INTO `cat_welding` (`id`, `product_id`) VALUES
(1, 2),
(2, 10),
(3, 20);

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `sender` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `text`, `sender`) VALUES
(1, 'The products of qianli, are not only products to repair mobile phones, they are sexy !!!\r\nthe design and the materials used are incredible, \r\nevery detail is studied including packaging, the care with which they are built reminds me of a famous Italian fashion designer\r\ncarefully designed for those who must carry out repairs every day, they make the life of cell phone repairers much easier\r\nEvery day new products every day new ideas,\r\nQianli is the example to follow to imitate, a young leader who is changing the way of thinking of Chinese companies', 'Savino - phoneparts');

-- --------------------------------------------------------

--
-- Structure de la table `downloads`
--

DROP TABLE IF EXISTS `downloads`;
CREATE TABLE IF NOT EXISTS `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `downloadlink` text NOT NULL,
  `menulink` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `downloads`
--

INSERT INTO `downloads` (`id`, `name`, `image`, `description`, `downloadlink`, `menulink`) VALUES
(1, 'iCopy', 'resources/iCopyImage.jpg', 'File Format ：msi / exe\r\nFile Size：180M\r\n\r\nApplicable device：QIANLI vibrator\r\nlight sense true-to-life color recovery device Q2.0\r\n\r\nVersion & model：V1.2.4\r\nAvailable For: Windows\r\n\r\nUpdating date：2019/8/3\r\n', 'link', 'link'),
(2, 'PCB Thermal camera', 'resources/PCBImage.jpg', 'File Format : ZIP\r\nFile Size：58M\r\n\r\nGraphic Format：JPG\r\nBPS：158kbps\r\n\r\nExtract Format：MP4\r\nVideo Resoulution：1920*1080\r\nFPS: 60.00FPS\r\nAudio Channel: 2(stereo)\r\nGraphic size：2.31M\r\n\r\nGraphic Resolution：2560*1956\r\n\r\nUpdating date：2019/7/20\r\n', 'link', 'link'),
(12, 'iCopy2', 'resources/iCopyImage.jpg', 'File Format ：msi / exe\r\nFile Size：180M\r\n\r\nApplicable device：QIANLI vibrator\r\nlight sense true-to-life color recovery device Q2.0\r\n\r\nVersion & model：V1.2.4\r\nAvailable For: Windows\r\n\r\nUpdating date：2019/8/3\r\n', 'link', 'link'),
(13, 'PCB Thermal camera3', 'resources/PCBImage.jpg', 'File Format : ZIP\r\nFile Size：58M\r\n\r\nGraphic Format：JPG\r\nBPS：158kbps\r\n\r\nExtract Format：MP4\r\nVideo Resoulution：1920*1080\r\nFPS: 60.00FPS\r\nAudio Channel: 2(stereo)\r\nGraphic size：2.31M\r\n\r\nGraphic Resolution：2560*1956\r\n\r\nUpdating date：2019/7/20\r\n', 'link', 'link'),
(11, 'PCB Thermal camera2', 'resources/PCBImage.jpg', 'File Format : ZIP\r\nFile Size：58M\r\n\r\nGraphic Format：JPG\r\nBPS：158kbps\r\n\r\nExtract Format：MP4\r\nVideo Resoulution：1920*1080\r\nFPS: 60.00FPS\r\nAudio Channel: 2(stereo)\r\nGraphic size：2.31M\r\n\r\nGraphic Resolution：2560*1956\r\n\r\nUpdating date：2019/7/20\r\n', 'link', 'link');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Image_folder` text NOT NULL,
  `Description` text NOT NULL,
  `video` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`ID`, `Name`, `Image_folder`, `Description`, `video`) VALUES
(1, 'iCopy', 'iCopy', 'Capable to restore original color without origin LCD. iCopy is a device to restore True Tone & Vibrator functions for cell phone after LCD & vibrator replacement. The PC terminal software can support True Tone function recovery withou original LCD, also available to back up & write in chip data. ', ''),
(2, '3D Screen Disassembler', '3DScreenDisassembler', 'The unique and ergonomic design makes it more comfortable and easy to grasp. Sensitive materials and precise polished process bring a pliable and tough disassembler. ', ''),
(3, 'iSee', 'iSee', 'Display any dust particles or scratches when used in cell phone and tablet repair and filming. This enables the technician to spot any dust or defects on LCD or Glass lens very quickly and ultimately speed up your work time. It also has a unique built-in work station with magnetic sections for holding screws, small parts and even your QianLi iThor Screwdrivers.', ''),
(6, '2D hard disk Black stencil for iphone', '2DharddiskBlackstencilforiphone', 'Perfectly fitting, Easily embedded in the black net，Fit the chip to the mesh，Thus, the chip after planting tin is more accurate, Efficient and precise positioning', ''),
(7, '3D Black stencil for android MSM-8953-B01-AB', '3DBlackstencilforandroidMSM8953B01AB', 'Qianli Square hole black stencils make you as stable as Mountain Tai,Black scrub absorbs glare \r\nvarious versions. Perfectly fitting, easily embedded in the black net，Fit the chip to the mesh，Thus, the chip after planting tin is more accurate, efficient and precise positioning', ''),
(8, 'iThor', 'iThor', '3D Screwdrivers tips\r\nPrecise and silent bearing\r\nRecognisable symbols on the bottom\r\nStrong magnetic metal tip. Fashionably designed with practical functions. \r\nThe handle is designed to prevent losing grip on screws with 3D tips which are carefully carved. \r\n\r\nWith its models\' mark on the bottom, it is easy to find during the operation process. \r\nMade with a strong magnetic metal, with long duration ensured. \r\nWorking with its quality silent bearing brings you a smooth & noise-free repair experience.', 'resources/videos/ithor.mp4'),
(9, 'PCB Thermal Camera', 'PCB', 'Detecting motherboard electricity leakage & abnormal heat\r\nAlso can use on PCB boards from other electronic products\r\nDetecting tiny variation of temperature(2~3 degree) and tiny current flow. (start with 7 mA) ', ''),
(10, 'Disassembler - Curved', 'Curved', 'Curved - A teardown piece designed for the removal of curved screens, made with ultra-flexible steel, support up to 90 degree bending without deforming designed to disassemble curved phone screen.', ''),
(11, '3D Positioning iPhoneA7- A11 CPU black stencil', '3DPositioningiPhoneA7-A11CPUblackstencil', 'Benefit from visual protection with the anti-reflective black color and material, that absorbs the light of your operations and keep your eyes safe.', ''),
(12, 'iPower MAX', 'iPowerMAX', 'Silicone flexible wire. iPower Max is a battery data simulator designed for chip-level maintenance on the cell phone.', ''),
(13, 'iSocket iPhone X', 'iSocketiPhoneX', 'Mini design, easy to carry. Special Premium quality material. Precise needle plate quality. High precision for holding iPhone X motherboard. Advanced inspection to avoid any damage because of repeated disassembly and installation. Easy to use, connect the upper and lower layer of iPhone motherboard for fast testing. iSocket X is a professional board diagnostic test supporting frame.With the iSocket, you can test both radio frequency boards and logic board without resoldering them each time.iSocket X/XS/XS Max are the best choice of double layers motherboard maintenance.', ''),
(14, '010 Ceramic Blades', '010', 'Color classification: 010 multi-function ceramic knife 010 tool holder + 1 blade of each', ''),
(15, 'MEGA-IDEA mini iSee', 'miniiSee', 'Brand Name: MEGA-IDEA\r\n\r\nPlace of Origin: Shenzhen city, Guangdong, China (Mainland)\r\n\r\nProduct Name: mini iSee\r\n\r\nApplication: Screen detection', ''),
(16, 'MEGA-IDEA Pointed Stainless Steel Tweezers', 'MEGAIDEATweezers', 'Brand Name: MEGA-IDEA\r\n\r\nPlace of Origin: Shenzhen city, Guangdong, China (Mainland)\r\n\r\nProduct Name: mini iSee\r\n\r\nApplication: Fixing tool, tweezers', ''),
(17, 'MEGA-IDEA Android CPU Black Stencil', 'MEGAIDEABlackStencil', 'rand Name: MEGA-IDEA\r\n\r\nProduct Name: Android black stencil\r\n\r\nColor: Black\r\n\r\nWeight:20g\r\n\r\nMade in China', ''),
(18, 'iBridge', 'iBridge', 'Brand Name: QianLi \r\n\r\nProduct Name: iBridge\r\n\r\nProduct series: 6G/6P/6S/6SP/7G/7P/8/8P/X\r\n\r\nSize:(mm)\r\n\r\nWeight:71g\r\n\r\nMade in China', ''),
(19, '3D Android Black Stencil Kirin655, 659-Hisilicon', '3DAndroidBlackStencilKirin655659Hisilicon', 'Applicable Brand: Android Mobile\r\nType: EMMC general DDR(Universal font temporary storage) MTK 6582(Media Tek) MSM 8917 2AA(Qualcomm Snapdragon) MSM 8937 2AA(Qualcomm Snapdragon) SDM 636 100-AA(Qualcomm Snapdragon) MSM8953 1AB(Qualcomm Snapdragon) MSM 8953 B01-AB(Qualcomm Snapdragon) MSM 8940 1AA(Qualcomm Snapdragon)  SDM 660 301-AA(Qualcomm Snapdragon)   MSM 8916 8909 8939 (Qualcomm Snapdragon)  MSM 8996(Qualcomm Snapdragon) Kirin655/659(Hisillicon) \r\nApplicable: 10021', ''),
(20, 'Bumblebee 936', 'bumblebee', 'Rapid warming，Multi-layer plating，Durable，Quality Assurance. The characteristic is Oxygen-free hole, Iron plating, Nickel plating, Chrome plating, Tinned protective layer; The advantage is good thermal conduction.\r\n\r\nThe rapid temperature rises fast. Durable. Anti-rust for the long term, High wear resistance, and hardness.\r\n\r\n', ''),
(21, 'iSocket iPhone Xs/Xs Max', 'iSocketiPhoneXsXsMax', 'Mini design, easy to carry.\r\n\r\nSpecial Premium quality material\r\n\r\nPrecision needle plate quality\r\n\r\nHigh precision for holding iPhone X/XS/XS Max motherboard\r\n\r\nAdvanced inspection to avoid any damage because of repeated disassembly and installation\r\n\r\nEasy to use, connect the upper and lower layer of iPhone X/XS/XS Max motherboard for fast testing', ''),
(22, 'iBrush', 'iBrush', 'Brushed with super ultra-fine steel wire\r\n\r\nThe back wire is bundled with bristles, neat and Aluminum alloy packaging design, more grades\r\n\r\na brush tail is magnetic', ''),
(23, 'Hot Bat-LP550', 'HotBat', 'Made with special material, resistant to deformation caused by temperature variations. \r\n\r\nVersatile : Usable with soldering iron tip or with a hot gun. \r\n\r\nMulti-shape chip slot allows to secure any type of chips. ', ''),
(24, 'iPry 1208', 'iPry', 'No harm to screen and cable\r\n\r\nSuper pliability and toughness \r\n\r\nEasy to use \r\n\r\nComfortable user experience  \r\n\r\n', ''),
(25, '009 Multifunction Glue Remover', '009', 'A box of twelve blades to solve various rubber removal problems\r\n\r\nDegumming/cutting/remove chips\r\n\r\nDouble head\r\n\r\nanti-slip groove\r\n\r\n', ''),
(26, '007 Glue Remover', '007', 'Noble Gold, 3 Blades and 1 Handle format\r\nSuitable for repairing and cleaning motherboard chips, cpu, baseband, disk\r\nHand Polished Blades\r\nNon-Slip Handle', ''),
(27, '008 Glue Remover', '008', '008 Glue Remover,  next realm of glue removal. \r\n\r\nMakes maintenance more concise, convenient and speedup reparation time. \r\n\r\nDuring maintainence tasks, we will encounter a variety of different issues, 008 can adopt for each use case in order to remove black glue more efectively.\r\n\r\nBlade polishing performed by superior technicians with more than 10 years of service. \r\n\r\nExperience pleasurable glue removal with improved sharpness and comfort of use.\r\n\r\n3 Blades 1 Handle format, one toolset to solve all problems.', ''),
(28, '3D Android Black Stencil MSM-8996-Qualcomm Snapdragon', '3DAndroidBlackStencilMSM8996QualcommSnapdragon', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(29, '3D Android Black Stencil MSM-8916-8909-8939-Qualcomm Snapdragon', '3DAndroidBlackStencilMSM891689098939QualcommSnapdragon', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(30, '3D Android Black Stencil SDM-660-301-AA Qualcomm Snapdragon', '3DAndroidBlackStencilSDM660301AAQualcommSnapdragon', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(31, '3D Android Black Stencil MSM-8940-1AA Qualcomm Snapdragon', '3DAndroidBlackStencilMSM89401AAQualcommSnapdragon', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(32, '3D Android Black Stencil MSM8953-1AB Qualcomm Snapdragon', '3DAndroidBlackStencilMSM8953-1ABQualcommSnapdragon', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(33, '3D Android Black Stencil MSM8953-1AB Qualcomm Snapdragon', '3DAndroidBlackStencilMSM89531ABQualcommSnapdragon', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(34, '3D Android Black Stencil MSM-8917-2AA', '3DAndroidBlackStencilMSM89172AA', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(35, '3D Android Black Stencil MTK-6582', '3DAndroidBlackStencilMTK6582', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(36, '3D Android Black Stencil MSM-8917-2AA+B37', '3DAndroidBlackStencilMSM89172AAB37', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(37, '3D Black Stencil for iPhone6/7/8 Hard Disk', '3DBlackStencilforiPhone678HardDisk', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(38, '3D Stencil for iPhone 5-8 power logic module', '3DStencilforiPhone58powerlogicmodule', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', ''),
(39, '3D Stencil for iPhone 5-8 baseband chips rework', '3DStencilforiPhone5-8basebandchipsrework', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', ''),
(40, '2D IC Gold Stencil  iphone6-XS', '2DICGoldStenciliphone6XS', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', ''),
(41, '2D Black stencil for iPhone A7-A12 CPU', '2DBlackstencilforiPhoneA7-A12CPU', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(42, '2D iPhone5-i8 Black Stencil', '2DiPhone5-i8BlackStencil', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(43, '2D iPhone5-i8 Black Stencil Communication baseband module', '2DiPhone5i8BlackStencilCommunicationbasebandmodule', 'square black holes\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions\r\n\r\n', ''),
(44, '3D IC iphone6-i8 gold Stencil', '3DICiphone6i8goldStencil', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', ''),
(45, '3D CPU A8-A11 Gold Stencil', '3DCPUA8-A11GoldStencil', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', ''),
(46, '2D CPU golden stencil for A8-A11', '2DCPUgoldenstencilforA8-A11', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', ''),
(47, '3D Android Black Stencil MSM-8937-2AAQualcomm Snapdragon', '3DAndroidBlackStencilMSM89372AAQualcommSnapdragon', '', ''),
(48, '2D CPU golden stencil for A8-A11', '2DCPUgoldenstencilforA8-A11', 'square gold hole\r\n\r\nAbsorbing light to protect eyes\r\n\r\nvarious versions', '');

-- --------------------------------------------------------

--
-- Structure de la table `timeline`
--

DROP TABLE IF EXISTS `timeline`;
CREATE TABLE IF NOT EXISTS `timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(100) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `timeline`
--

INSERT INTO `timeline` (`id`, `title`, `description`, `link`, `date`) VALUES
(1, 'Early times', 'QianLi Innovation Technology Co. was founded in 2016. We started deveolping fixing tool for mobile phone.', '', '2016'),
(2, 'Beginning to scale', 'First sales made after entering the fixing tools market', '', '2017'),
(3, 'Further development', 'We keep improving our products, by learning from our customer, and a game of trial and error.', '', '2018'),
(4, 'Rapid growind, birth of MEGA-IDEA', 'New sub-brand from QianLi, targeting lower price while keeping a similar repairing experience.', '', '2019');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
