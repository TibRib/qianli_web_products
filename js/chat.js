function collapseWindow(){
    var header = document.getElementById("panel-header");
    var chatBody = document.getElementById("panel-body");
    var chatFooter = document.getElementById("panel-footer");
    var minIcon = document.getElementById("icon_minim");

    if(chatBody.classList.contains("panel-collapsed")){
        chatBody.style.display = "block";
        chatFooter.style.display = "block";
        minIcon.innerHTML = '<i class="btncolor fa fa-minus-circle" aria-hidden="true"></i>';
        chatBody.classList.remove("panel-collapsed");
        IOconnect();
        header.style.cursor = "default";
    }
    else{
        chatBody.style.display = "none";
        chatFooter.style.display = "none";
        minIcon.innerHTML = '<i class="btncolor fa fa-plus-circle" aria-hidden="true"></i>';
        chatBody.classList.add("panel-collapsed");
        header.style.cursor = "pointer";
    }

}

var socket = 0;
var connected = false;
var room_id=0;

function fn(bool){
    alert(bool);
}

// socket code
// load socket.io-client
 function IOconnect(){
    socket = io.connect('127.0.0.1:3000');
    var body = document.getElementById("panel-body");

    //Here we get the latest room number to avoid getting in an occupied room
    //We then increment the number to make it our id


    socket.on('connect', function () {
        
        // socket connected
        socket.emit('room-number');

        socket.on('room-num-response', function(rep){
            console.log("rep : "+rep);
            socket.emit('room-leave', room_id );
            room_id = rep.amount+1;
            console.log("available room : "+ room_id);

            socket.emit('nickname', {"nickname" : String(room_id)});
            socket.emit('room-join', room_id);
            
            console.log("Connected successfully");

            socket.emit('room-list');
            connected = true;
        });

        socket.on('user message', function(message){
            if(parseInt(message.room) == parseInt(room_id)){ // TODO INT TO INT COMPARISONNNN
                var d = new Date();
                var content = ''
                + '<div class="col-md-10 col-xs-10">'
                +    '<div class="messages msg_receive">'
                +        '<p>'+ message.message +'</p>'
                +       '<time>'+ message.user +'• '+ d.getTime() +'</time>'
                +   '</div></div>';
        
                var e = document.createElement('div');
                e.className = "row msg_container base_receive";
                e.innerHTML = content;
        
                body.append(e);
            }
            else{
                console.log("Message difference with source room");
                console.log("Source : "+parseInt(message.room));
                console.log("Room : "+parseInt(room_id));
                console.log(message);
            }
        });

        socket.on('nicknames', function(nicknames) {
            var headTitle = document.getElementById("title-container");
            var count = Object.keys(nicknames).length;
            
        });
    
        socket.on('announcement', function(message){
            var content = ''
            + '<div class="col-md-10 col-xs-10">'
            +    '<div class="announce">'
            +        '<p>'+ message.action +'</p>'
            +   '</div></div>';
    
            var e = document.createElement('div');
            e.className = "row msg_container base_receive";
            e.innerHTML = content;
            console.log("'"+message.action+"'");
            console.log("joined room "+room_id.toString());
            if(message.action.endsWith("joined room "+room_id.toString())){
                document.getElementById('connectionStateCircle').style.color = "#15C428";
            }
            if(message.action.endsWith("quited room "+room_id.toString())){
                document.getElementById('connectionStateCircle').style.color = "#A22324";
            }
    
            
        });




      });

      socket.on('disconnect', function () {
        // socket disconnected
        console.log("Disconnected from the server");
        connected = false;
        document.getElementById('connectionStateCircle').style.color = "#A22324";


      });

    
    

    body.scrollTop = body.scrollHeight;

    console.log(socket);
}

function sendMessage(){
    var input = document.getElementById("txt-input");
    var text = input.value;
    console.log(text);

    if(connected == true && text != ''){
        socket.emit('user message',{"user":room_id, "message": text, "room":room_id});
        var body = document.getElementById("panel-body");
    
        var content = ''
            + '<div class="col-md-10 col-xs-10">'
            +    '<div class="messages msg_sent">'
            +        '<p>'+ text +'</p>'
            +       '<time datetime="2009-11-13T20:00"> me • 51 min</time>'
            +   '</div></div>';
    
            var e = document.createElement('div');
            e.className = "row msg_container base_sent";
            e.innerHTML = content;
    
        body.append(e);
        input.value = "";
    }
    
}
startTime();
function startTime() {
    var today = new Date();
    var h = today.getUTCHours() + 8;
    if(h >= 24){
        h-=24;
    }
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('timeChina').innerHTML =
    h + ":" + m + ":" + s + (h>12?" PM":" AM");
    var t = setTimeout(startTime, 500);
  }
  function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
  }

var typing = 0;
var txtTyped = document.getElementById("txt-input");
if(txtTyped){
    txtTyped.addEventListener('change', function(){
        var text = txtTyped.value;
        if(text.length > 0){
            if(typing == 0){
                console.log("Typing...");
                typing = 1;
                socket.emit('typing',{"room": 2});
            }
        }
        else
        {
            console.log("Stopped typing !");
            typing = 0;
        }
    }
    );
}
else{console.log("Null input");}