<?php
require_once 'vendor/autoload.php';
require_once 'Class/Database.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
$twig = new Twig_Environment($loader, [
    //'cache' => __DIR__ . '/tmp' //For final version
    'cache' => false
        ]);

$db = new Database('qianli','root','root');
$db->exec("SET CHARACTER SET utf8");

$page = 'home';
$twig->addGlobal('current_page', $page);


$id_product=null;

if(isset($_GET['page'])){
    $page=$_GET['page'];
}

if($page === 'product'){
    if(isset($_GET['id'])){
        $id_product=$_GET['id'];
    }
    else if(isset($_GET['name'])){
        $ret = $db->query("SELECT id FROM products WHERE Name LIKE '%".$_GET['name']."%' ");
        if(isset($ret[0]))
            $id_product= intval($ret[0]->id);
    }
}


//PAGES INFORMATIONS / META TAGS
$title = "QianLi";
$author = "QianLi";
$description = "QianLi is the brand of excellence in iPhone and phone repair tools : Products such as iThor , iSocket , Thermal camera";

/* --For future improvement of navbar that expands to reveal products 

require_once('Class/Category.php');
$products = $db->query("SELECT * FROM products");
$catdb =  $db->query("SELECT * FROM categories");

$categories9 = array();
foreach ($catdb as $cat) {
    $newc = new Category($cat->table,$cat->name,$db,'LIMIT 9');
    array_push($categories9, $newc );
}
$twig->addGlobal('categories9Nav', $categories9);
*/
//Make sure to set 'product_list' to true - on your pages


function renderCategoryPage($table, $name){
    global $db;
    global $twig;
    global $title, $author, $description;

    require_once('Class/Category.php');
    $cat_array = array( new Category($table,$name,$db));
    echo $twig->render("allproducts.twig",
    [
        'meta' => [
            'title' => $name.' - '.$title,
            'author' => $author,
            'description' => $description
        ],
        'categories' => $cat_array,
        'search' => false,
        'product_list' => false
    ]
    );
}

switch ($page) {
    case 'home':
        echo $twig->render("index.twig",
        [
            'meta' => [
                'title' => $title,
                'author' => $author,
                'description' => $description
            ],
            'product_list' => false
        ]
        );
        break;
    
    case 'about':
        //For the results : order by ASC or DESC
        $events = $db->query("SELECT * FROM timeline ORDER BY id ASC");
        echo $twig->render("about.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'product_list' => false,
            'timeline' => $events
        ]
        );
        break;
    
    case 'buying':
    case 'distributors':
        echo $twig->render("distributors.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'product_list' => false
        ]
        );
        break;
    
    case 'product':
        if(is_null($id_product) || $id_product <= 0){
            not_found:
            echo $twig->render("product-base.twig",
            [
                'meta' => [
                    'title' => $page.' - '.$title,
                    'author' => $author,
                    'description' => $description,
                ],
                'product' => [
                    'title' => 'Product not found',
                    'description' => 'The product you are looking for is not registered. Enjoy our placeholder content !',
                    'buy_link' => 'https://qianlispace.en.alibaba.com/',
                ],
                'index_link' => 'index.php',
                'product_list' => false
            ]
            );
        }
        else{
            //We get the product in the database
            $products = $db->query("SELECT * FROM products WHERE ID=".$id_product);
            if (array_key_exists(0,$products)==false) {
                goto not_found;
            }
            else{
                $product = $products[0];
                $img_folder = "resources/products/".$product->Image_folder;
                //For simplicity sake, $folder is our image array;
                $folder = array();
                if( is_dir($img_folder)){
                    $folder_base = scandir($img_folder,0);
                    for ($i=2; $i < sizeof($folder_base); $i++) {
                        if($folder_base[$i] != 'main.png')
                        array_push($folder,"resources/products/".$product->Image_folder."/".$folder_base[$i]);
                    }
                }
                if(isset($product->video)){
                    $video = $product->video;
                }else{
                    $video=false;
                }
                echo $twig->render("product.twig",
                [
                    'meta' => [
                        'title' => $product->Name.' - '.$title,
                        'author' => $author,
                        'description' => $description
                    ],
                    'product' => [
                        'title' => $product->Name,
                        'description' => $product->Description,
                        'buy_link' => 'https://qianlispace.en.alibaba.com/',
                        'video_link' => $video,
                        'mainPic' => 'resources/products/'.$product->Image_folder.'/main.png',
                        'folder' => $folder
                    ],
                    'index_link' => 'index.php',
                    'product_list' => false
                ]
                );
            }
        }
        break;
    
    case 'allproducts':
        require_once('Class/Category.php');
        $products = $db->query("SELECT * FROM products");
        $categories =  $db->query("SELECT * FROM categories");

        $cat_array = array();
        $registered_products = array();
        foreach ($categories as $cat) {
            $newc = new Category($cat->table,$cat->name,$db);
            array_push($cat_array, $newc );
            foreach ($newc->items as $item) {
                array_push($registered_products,$item->product_id);
            }
        }
        $unregistered_products = array();
        foreach ($products as $prod) {
            $in = false;
            foreach ($registered_products as $registered) {
               if ($prod->ID == $registered) {
                    $in = true;
               }
            }
            if($in == false){
                array_push($unregistered_products,$prod);
            }
        }
        echo $twig->render("allproducts.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'all' => $products,
            'categories' => $cat_array,
            'others' => $unregistered_products,
            'search' => true,
            'product_list' => false
        ]
        );
        break;


    case 'stencils':
        renderCategoryPage('cat_stencils',"Stencils");
    break;
    case 'removers':
        renderCategoryPage('cat_removers',"Blades and glue removal");
    break;
    case 'prying_tools':
        renderCategoryPage('cat_pryers',"Prying tools");
    break;
    case 'welding_tools':
        renderCategoryPage('cat_welding',"Welding and Soldering supplies");
    break;
    
    
    
    case 'downloads':
        $downloads = $db->query("SELECT * FROM downloads ORDER BY id DESC");
        echo $twig->render("downloads.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'downloads' => $downloads,
            'product_list' => false
        ]
        );
        break;

    case 'contact':
        $google_allowed = false;

        //We get the ip of the user -- is he from china ?
            if (isset($_SERVER['HTTP_CLIENT_IP']))
        {
            $real_ip_adress = $_SERVER['HTTP_CLIENT_IP'];
        }
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $real_ip_adress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $real_ip_adress = $_SERVER['REMOTE_ADDR'];
        }

        if (isset($real_ip_adress))
        {
            if(isset($_GET['country'])){
                $country = $_GET['country'];
                if($country == 'CN'){
                    $google_allowed = false;
                }
                else{
                    $google_allowed = true;
                }
            }
            else{
                $country = ip_info($real_ip_adress, "Country Code");
                if($country != "CN"){
                   $google_allowed = true;
                }
            }
        }
        
        echo $twig->render("contact.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'product_list' => false,
            'google_allowed' => $google_allowed
        ]
        );
        break;
    
    case 'insertproduct':
        $products = $db->query("SELECT * FROM products");
        echo $twig->render("insertproduct.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'products' => $products,
            'product_list' => false
        ]
        );
        break;
    
    case 'assigncategory':
        $products = $db->query("SELECT * FROM products");
        $categories =  $db->query("SELECT * FROM categories");
        echo $twig->render("assigncategory.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'products' => $products,
            'categories' => $categories,
            'product_list' => false
        ]
        );
        break;
    
    case 'comments':
        $comments = $db->query("SELECT * FROM comments");
        echo $twig->render("comments.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'comments' => $comments,
            'product_list' => false
        ]
        );
        break;
    
    default:
        echo $twig->render("404.twig",
        [
            'meta' => [
                'title' => $page.' - '.$title,
                'author' => $author,
                'description' => $description
            ],
            'product_list' => false
        ]
        );
        break;
}

//We try to know if the user ip is from China or Russia - Permits vpn to work
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}


?>